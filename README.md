# README #

This project is for presenting stuff related to JNLP : Java Network Launching Protocol aka WebStart.

### Nathan's Research ###

* It's also called WebStart
* Important in installing a JRE for the client in case he doesn't have Java installed
* [LINK](http://en.wikipedia.org/wiki/JNLP#Java_Network_Launching_Protocol_.28JNLP.29)
* A properly configured browser passes JNLP files to a Java Runtime Environment (JRE) which in turn downloads the application onto the user's machine and starts executing it.

### OB's Research ###
Useful Links:

*  [Java_Web_Start](http://en.wikipedia.org/wiki/Java_Web_Start)
* [JNLP Tutorial](http://docs.oracle.com/javase/tutorial/deployment/webstart/index.html)
* [JNLP example ](http://www.dmoz.org//Computers/Programming/Languages/Java/Development_Tools/Deployment/Java_Web_Start_and_JNLP)
* [JNLP ex. 2](https://today.java.net/pub/a/today/2005/08/11/webstart.html)
* [FAQ](https://www.java.com/en/download/faq/java_webstart.xml)